import expect from 'expect.js'
import Authsome from '../src'

// Below we setup a convincing journal user/fragment/collection/team situation

let admin = {
  id: 'user_1',
  name: 'Admin user',
  admin: true,
  type: 'user'
}

let admin2 = {
  id: 'user_2',
  name: 'Admin user 2',
  admin: true,
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Journal of Authsomeness',
  owners: [admin.id, admin2.id],
  type: 'collection'
}

let teams = {
  teamUsers: {
    name: 'Users',
    permissions: 'create'
  },
  teamReviewers: {
    name: 'Reviewers',
    permissions: 'read',
    active: function (object) {
      if (object.state === 'reviewing') {
        return true
      }
    }
  },
  teamCoauthors: {
    name: 'Coauthors',
    permissions: 'update'
  },
  teamEditors: {
    name: 'Editors',
    permissions: 'delete'
  }
}

let teamUsers = {
  id: 'team_1',
  object: collection,
  type: 'team',
  teamType: teams.teamUsers
}

let teamEditors = {
  id: 'team_3',
  object: collection,
  type: 'team',
  teamType: teams.teamEditors
}

let user3 = {
  id: 'user_3',
  name: 'User Three',
  teams: [teamUsers],
  type: 'user'
}

let user4 = {
  id: 'user_4',
  name: 'User Four',
  teams: [teamUsers]
}

let editor1 = {
  id: 'user_5',
  name: 'Editor of Journal of Authsomeness',
  teams: [teamUsers, teamEditors]
}

let collectionAdmin1 = {
  id: 'user_6',
  name: 'Collection Admin of Journal of Authsomeness',
  teams: [teamUsers]
}

let authsome = new Authsome('journal', {
  teams: teams
})

describe('Journal mode', function () {
  let manuscript = {
    id: 'fragment_1',
    title: 'Manuscript',
    owners: [user3.id],
    parents: [collection],
    state: 'writing',
    type: 'fragment'
  }

  describe('collection admins', function () {
    let submittedPaper = {
      id: 'fragment_2',
      title: 'Submitted paper',
      owners: [user4.id],
      parents: [collection],
      type: 'fragment',
      state: 'submitted'
    }

    before(function () {
      collection.owners.push(collectionAdmin1.id)
    })

    it('should be able to do anything if they are owners/collection admins of a parent collection', function () {
      var permission = authsome.can(collectionAdmin1, 'delete', submittedPaper)
      expect(permission).to.eql(true)
    })
  })

  describe('editors', function () {
    let submittedPaper = {
      id: 'fragment_2',
      title: 'Submitted paper',
      owners: [user4.id],
      parents: [collection],
      type: 'fragment',
      state: 'submitted'
    }

    it('should be able to read a submitted paper if they are editors of a parent collection', function () {
      var permission = authsome.can(editor1, 'read', submittedPaper)
      expect(permission).to.eql(true)
    })

    it('should not be able to read paper that has not yet been submitted, even if they are editors of a parent collection', function () {
      var permission = authsome.can(editor1, 'read', manuscript)
      expect(permission).to.eql(false)
    })
  })

  describe('users', function () {
    it('should be able to create a new paper/fragment', function () {
      var permission = authsome.can(user3, 'create', manuscript)
      expect(permission).to.eql(true)
    })
  })

  describe('coauthors', function () {
    before(function () {
      let teamCoauthors = {
        id: 'team_3',
        object: manuscript,
        type: 'team',
        teamType: teams.teamCoauthors
      }

      user3.teams = [teamCoauthors, teamUsers]
    })

    it('should be able to update the paper/fragment', function () {
      var permission = authsome.can(user3, 'update', manuscript)
      expect(permission).to.eql(true)
    })

    after(function () {
      user3.teams = [teamUsers]
    })
  })

  describe('reviewers', function () {
    describe('reviewing a manuscript', function () {
      before(function () {
        let teamReviewers = {
          id: 'team_2',
          object: manuscript,
          type: 'team',
          teamType: teams.teamReviewers
        }

        // Add user to teamReviewers (reviewers of fragment)
        user4.teams = [teamReviewers, teamUsers]
      })

      it('should only be able to read fragments when those fragments are in review and they are team members', function () {
        var permission = authsome.can(user4, 'read', manuscript)

        // Shouldn't be able to read it if it's not in state 'reviewing'
        expect(permission).to.eql(false)

        // Should be able to read it if it's in a state 'reviewing'
        manuscript.state = 'reviewing'
        permission = authsome.can(user4, 'read', manuscript)
        expect(permission).to.eql(true)
      })

      it('should not be able to delete fragments which they are reviewing', function () {
        var permission = authsome.can(user4, 'delete', manuscript)
        expect(permission).to.eql(false)
      })

      after(function () {
        user4.teams = [teamUsers]
      })
    })

    it('should be able to create fragments', function () {
      let review = {
        id: 'fragment_2',
        title: 'Review of Manuscript',
        owners: [user3.id],
        parents: [collection],
        type: 'fragment'
      }

      var permission = authsome.can(user4, 'create', review)
      expect(permission).to.eql(true)
    })
  })

  describe('public', function () {
    it('should only be able to read published fragments/papers', function () {
      manuscript.state = 'editing'
      var permission = authsome.can(undefined, 'read', manuscript)
      expect(permission).to.eql(false)

      manuscript.state = 'published'
      permission = authsome.can(undefined, 'read', manuscript)
      expect(permission).to.eql(true)
    })
  })
})
