import expect from 'expect.js'
import Authsome from '../src'

// CREATE UPDATE DELETE

let admin = {
  id: 'user_1',
  name: 'Admin user',
  admin: true,
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Blog',
  owners: [admin.id],
  type: 'collection'
}

let teams = {
  teamContributors: {
    name: 'Contributors',
    permissions: 'create'
  },
  teamCoauthors: {
    name: 'Coauthors',
    permissions: 'update'
  }
}

let teamContributors = {
  id: 'team_1',
  object: collection,
  type: 'team',
  teamType: teams.teamContributors
}

let user2 = {
  id: 'user_2',
  name: 'User One',
  teams: [teamContributors],
  type: 'user'
}

let fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

let teamCoauthors = {
  id: 'team_2',
  object: fragment,
  type: 'team',
  teamType: teams.teamCoauthors
}

let user3 = {
  id: 'user_3',
  name: 'User Two',
  teams: [teamCoauthors],
  type: 'user'
}

let fragment2 = {
  id: 'fragment_2',
  title: 'Post',
  owners: [user3.id],
  parents: [collection],
  type: 'fragment'
}

let fragment3 = {
  id: 'fragment_2',
  title: 'Post',
  owners: [user3.id],
  parents: [collection],
  type: 'fragment',
  published: true
}

describe('Blog mode', function () {
  describe('admin', function () {
    it('should be able to delete fragments owned by someone else', function () {
      var authsome = new Authsome()
      var permission = authsome.can(admin, 'delete', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to create fragments', function () {
      var authsome = new Authsome()
      var permission = authsome.can(admin, 'create', fragment)
      expect(permission).to.eql(true)
    })
  })

  describe('member of team contributors', function () {
    it('should be able to create fragments', function () {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      var permission = authsome.can(user2, 'create', fragment)
      expect(permission).to.eql(true)
    })

    it('should not be able to delete fragments which are owned by someone else', function () {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      var permission = authsome.can(user2, 'create', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to update fragments owned by them', function () {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      var permission = authsome.can(user2, 'update', fragment)
      expect(permission).to.eql(true)
    })

    it('should not be able to update fragments owned by other users', function () {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      var permission = authsome.can(user2, 'update', fragment2)
      expect(permission).to.eql(false)
    })
  })

  describe('member of team coauthors', function () {
    it('should be able to update the fragment', function () {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      var permission = authsome.can(user3, 'update', fragment)
      expect(permission).to.eql(true)
    })
  })

  describe('member of public', () => {
    it('should be able to access published fragments', () => {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      const permission = authsome.can(null, 'read', fragment3)
      expect(permission).to.eql(true)
    })

    it('should not be able to accessed unpublished fragments', () => {
      var authsome = new Authsome('blog', {
        teams: teams
      })

      const permission = authsome.can(null, 'read', fragment)
      expect(permission).to.eql(false)
    })
  })
})
