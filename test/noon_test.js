import expect from 'expect.js'
import Authsome from '../src'

let admin = {
  id: 'user_1',
  name: 'Admin user',
  admin: true,
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Journal of Afternooning',
  owners: [admin.id],
  type: 'collection'
}

let teams = {
  teamBeforenooners: {
    name: 'Beforenooners',
    permissions: 'all',
    active: function (p) {
      return (new Date()).getHours() <= 12
    }
  },
  teamAfternooners: {
    name: 'Afternooners',
    permissions: 'all',
    active: function (time) {
      return (new Date()).getHours() > 12
    }
  }
}

let teamBeforenooners = {
  id: 'team_1',
  object: collection,
  type: 'team',
  teamType: teams.teamBeforenooners
}

let user2 = {
  id: 'user_2',
  name: 'User One',
  teams: [teamBeforenooners],
  type: 'user'
}

let fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

let teamAfternooners = {
  id: 'team_2',
  object: collection,
  type: 'team',
  teamType: teams.teamAfternooners
}

let user3 = {
  id: 'user_3',
  name: 'User Two',
  teams: [teamAfternooners],
  type: 'user'
}

describe('Noon mode', function () {
  describe('Beforenooners', function () {
    it('should be able to create a fragment only if it\'s before noon, and not if it\'s after noon', function () {
      let beforeNoon = (new Date()).getHours() <= 12
      var authsome = new Authsome('noon', {
        teams: teams
      })
      var permission = authsome.can(user2, 'create', fragment)
      expect(permission).to.eql(beforeNoon)
    })
  })

  describe('Afternooners', function () {
    it('should be able to create a fragment only if it\'s after noon, and not if it\'s before noon', function () {
      let afterNoon = (new Date()).getHours() > 12
      var authsome = new Authsome('noon', {
        teams: teams
      })
      var permission = authsome.can(user3, 'create', fragment)
      expect(permission).to.eql(afterNoon)
    })
  })
})
