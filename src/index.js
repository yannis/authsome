'use strict'

// TODO: How to make this configurable
const canBlog = require('./blog')
const canNoon = require('./noon')
const canJournal = require('./journal')

class Authsome {
  constructor (mode, config) {
    this.mode = mode
    if (config) {
      this.teams = config.teams
    }

    if (!mode) {
      this.mode = 'freeforall'
      this.teams = undefined
    }
  }

  can (user, operation, object) {
    if (this.mode === 'blog') {
      return canBlog(user, operation, object)
    } else if (this.mode === 'noon') {
      return canNoon(user, operation, object)
    } else if (this.mode === 'journal') {
      return canJournal(user, operation, object)
    } else if (this.mode === 'freeforall') {
      return true
    }
  }
}

module.exports = Authsome
